//Maya ASCII 2015 scene
//Name: SM_CylinderLamp.ma
//Last modified: Tue, Apr 14, 2015 01:25:26 AM
//Codeset: 1252
requires maya "2015";
currentUnit -l centimeter -a degree -t ntsc;
fileInfo "application" "maya";
fileInfo "product" "Maya 2015";
fileInfo "version" "2015";
fileInfo "cutIdentifier" "201503261530-955654";
fileInfo "osv" "Microsoft Windows 8 , 64-bit  (Build 9200)\n";
fileInfo "license" "student";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 19.090449446840331 -15.271467849540102 -108.44824537830407 ;
	setAttr ".r" -type "double3" -2.1383527300504426 -3066.1999999981203 0 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".coi" 102.59901309614602;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".tp" -type "double3" 6.3861265182495117 -16.762094990997348 0 ;
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 6.3861265182495117 500.30018354784278 1.1481088935979323e-013 ;
	setAttr ".r" -type "double3" -89.999999999999986 0 0 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 500.1;
	setAttr ".ow" 38.605799027493134;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 6.3861265182495117 -16.762094990997348 500.67477767702746 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 500.1;
	setAttr ".ow" 74.869472721040992;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 500.8937372248223 -16.762094990997348 1.0980274705176211e-013 ;
	setAttr ".r" -type "double3" 0 89.999999999999986 0 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".coi" 500.1;
	setAttr ".ow" 74.765052256994892;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "pCylinder3";
	setAttr ".t" -type "double3" 0 -26.32211591461185 0 ;
	setAttr ".rp" -type "double3" 0 26.32211591461185 0 ;
	setAttr ".sp" -type "double3" 0 26.32211591461185 0 ;
createNode mesh -n "pCylinder3Shape" -p "pCylinder3";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.52282031625509262 0.50019609928131104 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".uvst[1].uvsn" -type "string" "lightmap";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 66 ".pt";
	setAttr ".pt[0]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[1]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[2]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[3]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[4]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[5]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[6]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[7]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[8]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[9]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[10]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[11]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[12]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[13]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[14]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[15]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[16]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[17]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[18]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[19]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[20]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[21]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[22]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[23]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[24]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[25]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[26]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[27]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[28]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[29]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[30]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[31]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[32]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[33]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[34]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[35]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[36]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[37]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[38]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[39]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[40]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[41]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[42]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[43]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[44]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[45]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[46]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[47]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[48]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[49]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[50]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[51]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[52]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[53]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[54]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[55]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[56]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[57]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[66]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[67]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[68]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[69]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[70]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[71]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[72]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".pt[73]" -type "float3" 0 -8.9162493 0 ;
	setAttr ".bw" 3;
createNode mesh -n "polySurfaceShape1" -p "pCylinder3";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".pv" -type "double2" 0.35173679888248444 0.94110071659088135 ;
	setAttr -s 2 ".uvst";
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 108 ".uvst[0].uvsp[0:107]" -type "float2" 0.5107196 0.71329218
		 0.51071954 0.61167753 0.59795624 0.61167765 0.59795624 0.71329218 0.51071966 0.81490678
		 0.59795618 0.81490678 0.5107196 0.71329218 0.51071954 0.61167753 0.51071954 0.51006305
		 0.59795618 0.51006305 0.59795618 0.10360482 0.85612828 0.73409146 0.90125847 0.84304601
		 0.74101043 0.84304607 0.51071966 0.81490678 0.51071954 0.51006305 0.39511174 0.71329212
		 0.39511177 0.61167759 0.74717367 0.68896109 0.5107196 0.40844852 0.59795624 0.40844852
		 0.5107196 0.20521939 0.59795624 0.20521939 0.85612833 0.95200074 0.39511168 0.81490684
		 0.5107196 0.40844852 0.39511177 0.51006311 0.63821906 0.7340914 0.28459477 0.71329212
		 0.28459477 0.61167753 0.2845948 0.61167753 0.28459477 0.71329212 0.5107196 0.30683392
		 0.59795618 0.30683392 0.5107196 0.20521939 0.74717367 0.99713135 0.59308851 0.84304601
		 0.35716039 0.839261 0.33970511 0.86571407 0.28459477 0.81490684 0.44144255 0.83926105
		 0.28459477 0.81490684 0.5107196 0.30683392 0.39511177 0.40844852 0.28459477 0.51006305
		 0.28459477 0.51006305 0.0019921064 0.71329212 0.0019920766 0.61167741 0.39511174
		 0.20521939 0.39511177 0.10360485 0.63821912 0.95200068 0.28459477 0.10360479 0.0019921064
		 0.8149066 0.39511177 0.30683392 0.28459477 0.40844846 0.28459477 0.40844846 0.0019920617
		 0.51006293 0.28459477 0.20521939 0.68312335 0.56207067 0.7292372 0.45074224 0.83426803
		 0.56207067 0.28459477 0.20521939 0.84056562 0.40462828 0.28459477 0.30683386 0.28459477
		 0.30683386 0.7292372 0.67339909 0.0019920692 0.40844846 0.95189428 0.45074221 0.0019921064
		 0.20521939 0.84056556 0.71951312 0.99800801 0.56207067 0.95189428 0.67339909 0.60287499
		 0.4116379 0.80706334 0.4116379 0.80706352 0.054307938 0.60287499 0.10535514 0.80706352
		 0.10535502 0.60287499 0.36059076 0.80706334 0.3605907 0.60287499 0.15640223 0.80706352
		 0.15640223 0.60287499 0.30954361 0.80706334 0.30954361 0.60287493 0.20744938 0.80706352
		 0.20744932 0.60287499 0.25849646 0.80706334 0.25849652 0.28459477 0.0019903183 0.35716045
		 0.079250515 0.0019920766 0.30683398 0.3993015 0.089703083 0.44144246 0.079250515
		 0.39930141 0.82880843 0.51071954 0.0019902587 0.59795624 0.0019902885 0.5107196 0.10360485
		 0.51071954 0.0019902587 0.45889783 0.052797556 0.44144243 0.026344717 0.28459477
		 0.0019903183 0.0019921064 0.10360479 0.0019919872 0.0019903183 0.28459477 0.10360479
		 0.5107196 0.10360485 0.33970505 0.052797556 0.60287499 0.054307938 0.60287499 0.0032608509
		 0.80706346 0.0032608509;
	setAttr ".uvst[1].uvsn" -type "string" "lightmap";
	setAttr -s 108 ".uvst[1].uvsp[0:107]" -type "float2" 0.44983441 0.82602775
		 0.45370495 0.71218228 0.51625407 0.71341252 0.51259303 0.82655406 0.4490028 0.93775618
		 0.51164722 0.93723583 0.43607122 0.82624245 0.43959886 0.71190774 0.45600185 0.59798765
		 0.51862335 0.59897113 0.85612828 0.73409146 0.90125847 0.84304601 0.74101043 0.84304607
		 0.74717367 0.68896109 0.45751548 0.14194807 0.46985939 0.032355636 0.53205597 0.038479418
		 0.52058446 0.14629763 0.43471533 0.94429374 0.4425742 0.59779894 0.35366365 0.82550657
		 0.35713175 0.71035504 0.45763874 0.48361486 0.52018923 0.48416689 0.85612833 0.95200074
		 0.74717367 0.99713135 0.45717224 0.25538698 0.51947576 0.25631267 0.44355667 0.14068067
		 0.45633093 0.024741441 0.63821906 0.7340914 0.59308851 0.84304601 0.34972885 0.94364667
		 0.44385713 0.48364082 0.35970998 0.59645808 0.26098686 0.81987381 0.26434389 0.70742917
		 0.27847505 0.7079078 0.2747722 0.82110417 0.45776752 0.36938867 0.52023995 0.36963102
		 0.44304216 0.25511861 0.3199816 0.97173905 0.30576736 1.0012571812 0.26875827 0.9379077
		 0.40349275 0.04755047 0.41209298 0.078992307 0.25498205 0.93044353 0.44397926 0.36945578
		 0.36116484 0.48285881 0.26707432 0.59472811 0.28055149 0.59498179 0.058165871 0.81033385
		 0.061868764 0.70077074 0.63821912 0.95200068 0.3608796 0.25427693 0.359577 0.13716283
		 0.26541793 0.1436021 0.25721192 0.033934355 0.27079937 0.026578039 0.27938852 0.14222583
		 0.052792318 0.91938245 0.36144322 0.36901394 0.26830554 0.48201004 0.28212065 0.48225662
		 0.064416327 0.5909456 0.68312335 0.56207067 0.7292372 0.45074224 0.83426803 0.56207067
		 0.26788223 0.25634086 0.28203088 0.25600863 0.061799474 0.15298086 0.055647902 0.044154197
		 0.84056562 0.40462828 0.26859602 0.36923131 0.28241852 0.36929232 0.06573341 0.48100552
		 0.7292372 0.67339909 0.064266361 0.26183268 0.95189428 0.45074221 0.06584876 0.37119934
		 0.84056556 0.71951312 0.99800801 0.56207067 0.95189428 0.67339909 0.60287499 0.054307938
		 0.60287499 0.0032608509 0.80706346 0.0032608509 0.80706352 0.054307938 0.60287499
		 0.10535514 0.80706352 0.10535502 0.60287499 0.4116379 0.60287499 0.36059076 0.80706334
		 0.3605907 0.80706334 0.4116379 0.60287499 0.15640223 0.80706352 0.15640223 0.60287499
		 0.30954361 0.80706334 0.30954361 0.60287493 0.20744938 0.80706352 0.20744932 0.60287499
		 0.25849646 0.80706334 0.25849652 0.36278516 0.11978504 0.39566663 0.10798186 0.35212329
		 0.96120667 0.383035 0.97415376 0.33004689 0.10895443 0.31478602 0.080591679;
	setAttr ".cuvs" -type "string" "lightmap";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 33 ".pt";
	setAttr ".pt[0]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[1]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[4]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[6]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[7]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[8]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[11]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[13]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[14]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[15]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[16]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[17]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[19]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[21]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[22]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[23]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[24]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[25]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[26]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[27]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[29]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[30]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[33]" -type "float3" 1.1920929e-006 -2.0861626e-007 0 ;
	setAttr ".pt[39]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[40]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[41]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[42]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[45]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[46]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[48]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[49]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[52]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr ".pt[54]" -type "float3" 6.5565109e-007 0 0 ;
	setAttr -s 74 ".vt[0:73]"  -5.60863113 3.82683372 -9.23879242 -5.60863113 -3.82683372 -9.23879433
		 -10 -3.82683372 -9.23879433 -10 3.82683372 -9.23879242 -5.60863113 9.23879623 -3.82683396
		 -10 9.23879623 -3.82683396 -5.60863113 4.2375145 -10.23026657 -5.60863113 -4.2375145 -10.23026752
		 -5.60863113 -9.23879242 -3.82683325 -10 -9.23879242 -3.82683325 -10 0 4.1633363e-016
		 -5.60863113 9.23879433 3.82683349 -10 9.23879433 3.82683349 -5.60863113 10.23027039 -4.2375164
		 -5.60863113 -10.23026657 -4.23751545 5.77417564 4.2375145 -10.23026657 5.77417564 -4.2375145 -10.23026752
		 -5.60863113 -9.23879623 3.82683396 -10 -9.23879623 3.82683396 -5.60863113 3.82683372 9.23879433
		 -10 3.82683372 9.23879433 -5.60863113 10.23027039 4.23751593 5.77417564 10.23027039 -4.2375164
		 -5.60863113 -10.23026657 4.23751545 5.77417564 -10.23026657 -4.23751545 5.77417564 3.82683372 -9.23879242
		 5.77417564 -3.82683372 -9.23879433 -5.60863113 -3.82683372 9.23879242 -10 -3.82683372 9.23879242
		 -5.60863113 4.2375145 10.23026752 5.77417564 10.23027039 4.23751593 2.12132025 10.23027039 -2.12132025
		 3 10.23027039 0 2.12132025 10.23027039 2.12132025 0 10.23027039 2.99999976 -2.12132025 10.23027039 2.12132025
		 -2.99999952 10.23027039 0 -2.12132025 10.23027039 -2.12132025 0 10.23027039 -2.99999952
		 5.77417564 9.23879623 -3.82683396 -5.60863113 -4.2375145 10.23026657 5.77417564 -10.23026657 4.23751545
		 5.77417564 -9.23879242 -3.82683325 20 3.82683372 -9.23879242 20 -3.82683372 -9.23879433
		 5.77417564 4.2375145 10.23026752 5.77417564 9.23879433 3.82683349 20 9.23879623 -3.82683396
		 5.77417564 -4.2375145 10.23026657 5.77417564 -9.23879623 3.82683396 20 -9.23879242 -3.82683325
		 22.77225304 0 -4.1633363e-016 5.77417564 3.82683372 9.23879433 20 9.23879433 3.82683349
		 5.77417564 -3.82683372 9.23879242 20 -9.23879623 3.82683396 20 3.82683372 9.23879433
		 20 -3.82683372 9.23879242 0 29.35030937 -2.99999952 2.12132025 29.35030937 -2.12132025
		 3 29.35030937 0 -2.12132025 29.35030937 -2.12132025 2.12132025 29.35030937 2.12132025
		 -2.99999952 29.35030937 0 0 29.35030937 2.99999976 -2.12132025 29.35030937 2.12132025
		 0.21090508 10.23027039 -4.2375164 0.21090508 4.2375145 -10.23026657 0.21090508 -4.2375145 -10.23026752
		 0.21090508 -10.23026657 -4.23751545 0.21090508 -10.23026657 4.23751545 0.21090508 -4.2375145 10.23026657
		 0.21090508 4.2375145 10.23026752 0.21090508 10.23027039 4.23751593;
	setAttr -s 147 ".ed[0:146]"  0 1 0 1 2 0 2 3 0 3 0 0 4 0 0 3 5 0 5 4 0
		 0 6 1 6 7 0 7 1 1 1 8 0 8 9 0 9 2 0 2 10 1 11 4 0 5 12 0 12 11 0 4 13 1 13 6 0 10 5 1
		 7 14 0 14 8 1 15 16 0 16 68 0 6 67 0 8 17 0 17 18 0 18 9 0 19 11 0 12 20 0 20 19 0
		 11 21 1 21 13 0 22 15 0 13 66 0 14 23 0 23 17 1 16 24 0 24 69 0 25 26 0 26 16 1 15 25 1
		 17 27 0 27 28 0 28 18 0 18 10 1 27 19 0 20 28 0 19 29 1 29 21 0 10 20 1 31 32 0 32 33 0
		 33 34 0 34 35 0 35 36 0 36 21 0 21 73 0 30 22 0 36 37 0 37 38 0 38 31 0 39 25 0 22 39 1
		 23 40 0 40 27 1 24 41 0 41 70 0 26 42 0 42 24 1 43 44 0 44 26 0 25 43 0 40 29 0 45 30 0
		 29 72 0 46 39 0 30 46 1 47 43 0 39 47 0 41 48 0 48 71 0 42 49 0 49 41 1 44 50 0 50 42 0
		 43 51 0 51 44 0 48 45 0 52 46 0 45 52 1 53 47 0 46 53 0 47 51 0 49 54 0 54 48 1 50 55 0
		 55 49 0 51 50 0 54 52 0 56 53 0 52 56 0 53 51 0 55 57 0 57 54 0 51 55 0 57 56 0 56 51 0
		 51 57 0 38 58 0 58 59 0 59 31 0 59 60 0 60 32 0 37 61 0 61 58 0 60 62 0 62 33 0 36 63 0
		 63 61 0 62 64 0 64 34 0 35 65 0 65 63 0 64 65 0 66 22 0 67 15 0 66 67 0 68 7 0 67 68 0
		 69 14 0 68 69 0 70 23 0 69 70 0 71 40 0 70 71 0 72 45 0 71 72 0 73 30 0 72 73 0 34 73 0
		 38 66 0 31 22 0 37 13 0 33 30 0 35 21 0 32 22 0;
	setAttr -s 247 ".n";
	setAttr ".n[0:165]" -type "float3"  0 0.38268343 -0.92387956 0 -0.38268346
		 -0.92387956 0 -0.38268346 -0.92387956 0 0.38268343 -0.92387956 0 0.92387956 -0.38268352
		 0 0.38268343 -0.92387956 0 0.38268343 -0.92387956 0 0.92387956 -0.38268352 1e+020
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 0 -0.38268346
		 -0.92387956 0 -0.92387956 -0.38268346 0 -0.92387956 -0.38268346 0 -0.38268346 -0.92387956
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 0 0.92387956 0.38268349 0 0.92387956 -0.38268352 0 0.92387956 -0.38268352 0 0.92387956
		 0.38268349 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 0 0.38268331 -0.9238795 0 -0.38268349 -0.92387956 0 -0.38268343 -0.9238795
		 0 0.38268331 -0.9238795 0 -0.92387956 -0.38268346 0 -0.92387956 0.38268346 0 -0.92387956
		 0.38268346 0 -0.92387956 -0.38268346 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 0
		 0.38268334 0.92387956 0 0.92387956 0.38268349 0 0.92387956 0.38268349 0 0.38268334
		 0.92387956 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 0 0.92387956 -0.38268349 0 0.38268331 -0.9238795 0 0.38268331 -0.9238795
		 0 0.96753818 -0.25272489 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 0 -0.38268349
		 -0.92387956 0 -0.92387956 -0.38268343 0 -0.92387956 -0.38268343 0 -0.38268343 -0.9238795
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 0 -0.92387956 0.38268346 0 -0.38268346 0.92387956 0 -0.38268346 0.92387956 0 -0.92387956
		 0.38268346 0 -0.38268346 0.92387956 0 0.38268334 0.92387956 0 0.38268334 0.92387956
		 0 -0.38268346 0.92387956 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 0 1 1.738513e-007
		 0 1 0 0 0.98229027 -0.18736556 0 1 2.114847e-008 0 0.96753818 -0.25272489 0 0.98229027
		 0.18736561 0 1 -7.6001648e-008 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 1e+020 1e+020 0 -0.92387956 -0.38268343 0 -0.92387956 0.38268352 0
		 -0.92387956 0.38268352 0 -0.92387956 -0.38268343 1e+020 1e+020 1e+020 1e+020 1e+020
		 1e+020 0 0.38268343 -0.92387956 0 -0.38268346 -0.92387956 0 -0.38268346 -0.92387956
		 0 0.38268343 -0.92387956 1e+020 1e+020 1e+020 0 0.38268325 0.92387962 0 0.92387944
		 0.38268352 0 0.98229027 0.18736561 0 0.38268331 0.92387962 1e+020 1e+020 1e+020 1e+020
		 1e+020 1e+020 0 0.92387956 -0.38268352 0 0.38268343 -0.92387956 0 0.38268343 -0.92387956
		 0 0.92387956 -0.38268352 0 -0.92387956 0.38268352 0 -0.38268346 0.92387956 0 -0.38268343
		 0.92387956 0 -0.92387956 0.38268352 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 0 -0.38268346
		 -0.92387956 0 -0.92387956 -0.38268337 0 -0.92387956 -0.38268337 0 -0.38268346 -0.92387956
		 0.91910315 -0.15078373 -0.36402428 0.91910309 0.150784 -0.36402443 1 1.1720731e-008
		 4.6906226e-009 0 -0.38268346 0.92387956 0 0.38268325 0.92387962 0 0.38268331 0.92387962
		 0 -0.38268343 0.92387956 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 0 0.92387956 0.38268352
		 0 0.92387956 -0.38268352 0 0.92387956 -0.38268352 0 0.92387956 0.38268352 0.91910309
		 0.150784 -0.36402443 0.91910315 0.36402422 -0.15078387 1 1.1720731e-008 4.6906226e-009
		 1e+020 1e+020 1e+020 1e+020 1e+020 1e+020 0 -0.92387956 -0.38268337 0 -0.9238795
		 0.38268343 0 -0.9238795 0.38268343 0 -0.92387956 -0.38268337 0.91910315 -0.36402428
		 -0.15078387 0.91910315 -0.15078373 -0.36402428 1 1.1720731e-008 4.6906226e-009 0
		 0.38268334 0.92387956 0 0.92387956 0.38268352 0 0.92387956 0.38268352 0 0.38268334
		 0.92387956 0.91910315 0.36402422 -0.15078387 0.91910315 0.36402431 0.15078387 1 1.1720731e-008
		 4.6906226e-009 0 -0.9238795 0.38268343 0 -0.38268352 0.92387956 0 -0.38268352 0.92387956
		 0 -0.9238795 0.38268343 0.91910321 -0.36402422 0.15078384 0.91910315 -0.36402428
		 -0.15078387 1 1.1720731e-008 4.6906226e-009 0 -0.38268352 0.92387956 0 0.38268334
		 0.92387956 0 0.38268334 0.92387956 0 -0.38268352 0.92387956 0.91910315 0.36402431
		 0.15078387 0.91910315 0.15078372 0.36402437 1 1.1720731e-008 4.6906226e-009 0.91910309
		 -0.15078399 0.3640244 0.91910321 -0.36402422 0.15078384 1 1.1720731e-008 4.6906226e-009
		 0.91910315 0.15078372 0.36402437 0.91910309 -0.15078399 0.3640244 1 1.1720731e-008
		 4.6906226e-009 0.70710671 0 -0.70710683 0 0 -1;
	setAttr ".n[166:246]" -type "float3"  0 0 -1 0.70710671 0 -0.70710683 1 0 0
		 0.70710671 0 -0.70710683 0.70710671 0 -0.70710683 1 0 0 0 0 -1 -0.70710683 0 -0.70710677
		 -0.70710683 0 -0.70710677 0 0 -1 0.70710677 0 0.70710683 1 0 0 1 0 0 0.70710677 0
		 0.70710683 -0.70710683 0 -0.70710677 -0.99999994 0 0 -0.99999994 0 0 -0.70710683
		 0 -0.70710677 0 0 1 0.70710677 0 0.70710683 0.70710677 0 0.70710683 0 0 1 -0.99999994
		 0 0 -0.70710683 0 0.70710677 -0.70710683 0 0.70710677 -0.99999994 0 0 -0.70710683
		 0 0.70710677 0 0 1 0 0 1 -0.70710683 0 0.70710677 0 0.98229027 -0.18736556 0 0.38268337
		 -0.92387956 0 0.38268331 -0.9238795 0 0.92387956 -0.38268349 0 0.38268337 -0.92387956
		 0 -0.3826834 -0.9238795 0 -0.38268349 -0.92387956 0 0.38268331 -0.9238795 0 -0.3826834
		 -0.9238795 0 -0.92387956 -0.38268343 0 -0.92387956 -0.38268343 0 -0.38268349 -0.92387956
		 0 -0.92387956 -0.38268343 0 -0.92387956 0.38268343 0 -0.92387956 0.38268352 0 -0.92387956
		 -0.38268343 0 -0.92387956 0.38268343 0 -0.38268352 0.92387956 0 -0.38268346 0.92387956
		 0 -0.92387956 0.38268352 0 -0.38268352 0.92387956 0 0.38268331 0.92387962 0 0.38268325
		 0.92387962 0 -0.38268346 0.92387956 0 0.38268331 0.92387962 0 0.96753818 0.2527248
		 0 0.92387944 0.38268352 0 0.38268325 0.92387962 0 0.92387944 0.38268352 0 1 0 0 1
		 1.3565236e-007 0 0.98229027 0.18736561 0 1 1.738513e-007 0 0.98229027 -0.18736556
		 0 0.92387956 -0.38268349 0 1 2.5991562e-007 0 1 2.5991562e-007 0 0.92387956 -0.38268349
		 0 0.96753818 -0.25272489 0 1 2.114847e-008 0 0.96753818 0.2527248 0 1 0 0 1 0 0 0.92387944
		 0.38268352 0 0.98229027 0.18736561 0 1 1.3565236e-007 0 1 -7.6001648e-008 0 0.98229027
		 -0.18736556 0 1 0 0 1 0 0 0.96753818 0.2527248;
	setAttr -s 74 -ch 286 ".fc[0:73]" -type "polyFaces" 
		f 4 0 1 2 3
		mu 0 4 0 1 2 3
		mu 1 4 0 1 2 3
		f 4 4 -4 5 6
		mu 0 4 4 0 3 5
		mu 1 4 4 0 3 5
		f 4 -1 7 8 9
		mu 0 4 1 0 6 7
		mu 1 4 1 0 6 7
		f 4 10 11 12 -2
		mu 0 4 1 8 9 2
		mu 1 4 1 8 9 2
		f 4 -3 13 19 -6
		mu 0 4 11 12 13 18
		mu 1 4 10 11 12 13
		f 4 14 -7 15 16
		mu 0 4 95 93 94 10
		mu 1 4 14 15 16 17
		f 4 -5 17 18 -8
		mu 0 4 0 4 14 6
		mu 1 4 0 4 18 6
		f 4 -11 -10 20 21
		mu 0 4 8 1 7 15
		mu 1 4 8 1 7 19
		f 4 129 128 -9 24
		mu 0 4 16 17 7 6
		mu 1 4 20 21 7 6
		f 4 25 26 27 -12
		mu 0 4 8 19 20 9
		mu 1 4 8 22 23 9
		f 4 -14 -13 -28 45
		mu 0 4 13 12 23 35
		mu 1 4 12 11 24 25
		f 4 28 -17 29 30
		mu 0 4 21 95 10 22
		mu 1 4 26 14 17 27
		f 4 -15 31 32 -18
		mu 0 4 93 95 103 96
		mu 1 4 15 14 28 29
		f 4 -16 -20 50 -30
		mu 0 4 27 18 13 36
		mu 1 4 30 13 12 31
		f 4 127 -25 -19 34
		mu 0 4 24 16 6 14
		mu 1 4 32 20 6 18
		f 4 -26 -22 35 36
		mu 0 4 19 8 15 25
		mu 1 4 22 8 19 33
		f 4 131 130 -21 -129
		mu 0 4 17 26 15 7
		mu 1 4 21 34 19 7
		f 4 39 40 -23 41
		mu 0 4 28 29 30 31
		mu 1 4 35 36 37 38
		f 4 42 43 44 -27
		mu 0 4 19 32 33 20
		mu 1 4 22 39 40 23
		f 4 46 -31 47 -44
		mu 0 4 32 21 22 33
		mu 1 4 39 26 27 40
		f 4 -29 48 49 -32
		mu 0 4 95 21 34 103
		mu 1 4 14 26 41 28
		f 3 51 146 -143
		mu 0 3 37 38 39
		mu 1 3 42 43 44
		f 4 143 -33 -57 59
		mu 0 4 98 96 103 97
		mu 1 4 45 29 28 46
		f 4 62 -42 -34 63
		mu 0 4 41 28 31 39
		mu 1 4 47 35 38 44
		f 4 -43 -37 64 65
		mu 0 4 32 19 25 42
		mu 1 4 39 22 33 48
		f 4 133 132 -36 -131
		mu 0 4 26 43 25 15
		mu 1 4 34 49 33 19
		f 4 68 69 -38 -41
		mu 0 4 29 44 45 30
		mu 1 4 36 50 51 37
		f 4 70 71 -40 72
		mu 0 4 46 47 29 28
		mu 1 4 52 53 36 35
		f 4 -46 -45 -48 -51
		mu 0 4 13 35 50 36
		mu 1 4 12 25 54 31
		f 4 -47 -66 73 -49
		mu 0 4 21 32 42 34
		mu 1 4 26 39 48 41
		f 4 139 -58 -50 75
		mu 0 4 48 49 103 34
		mu 1 4 55 56 28 41
		f 4 76 -64 -59 77
		mu 0 4 102 99 87 51
		mu 1 4 57 58 59 60
		f 4 78 -73 -63 79
		mu 0 4 52 46 28 41
		mu 1 4 61 52 35 47
		f 4 135 134 -65 -133
		mu 0 4 43 53 42 25
		mu 1 4 49 62 48 33
		f 4 82 83 -67 -70
		mu 0 4 44 54 55 45
		mu 1 4 50 63 64 51
		f 4 84 85 -69 -72
		mu 0 4 47 56 44 29
		mu 1 4 53 65 50 36
		f 3 -71 86 87
		mu 0 3 58 59 60
		mu 1 3 66 67 68
		f 4 137 -76 -74 -135
		mu 0 4 53 48 34 42
		mu 1 4 62 55 41 48
		f 4 89 -78 -75 90
		mu 0 4 57 102 51 61
		mu 1 4 69 57 60 70
		f 4 91 -80 -77 92
		mu 0 4 100 101 99 102
		mu 1 4 71 72 58 57
		f 3 -79 93 -87
		mu 0 3 59 62 60
		mu 1 3 67 73 68
		f 4 94 95 -81 -84
		mu 0 4 54 63 64 55
		mu 1 4 63 74 75 64
		f 4 96 97 -83 -86
		mu 0 4 56 66 54 44
		mu 1 4 65 76 63 50
		f 3 -85 -88 98
		mu 0 3 65 58 60
		mu 1 3 77 66 68
		f 4 99 -91 -89 -96
		mu 0 4 63 57 61 64
		mu 1 4 74 69 70 75
		f 4 100 -93 -90 101
		mu 0 4 68 100 102 57
		mu 1 4 78 71 57 69
		f 3 -92 102 -94
		mu 0 3 62 67 60
		mu 1 3 73 79 68
		f 4 103 104 -95 -98
		mu 0 4 66 89 63 54
		mu 1 4 76 80 74 63
		f 3 -97 -99 105
		mu 0 3 69 65 60
		mu 1 3 81 77 68
		f 4 106 -102 -100 -105
		mu 0 4 89 68 57 63
		mu 1 4 80 78 69 74
		f 3 -101 107 -103
		mu 0 3 67 70 60
		mu 1 3 79 82 68
		f 3 -104 -106 108
		mu 0 3 71 69 60
		mu 1 3 83 81 68
		f 3 -107 -109 -108
		mu 0 3 70 71 60
		mu 1 3 82 83 68
		f 4 -62 109 110 111
		mu 0 4 105 106 107 74
		mu 1 4 84 85 86 87
		f 4 -52 -112 112 113
		mu 0 4 75 105 74 76
		mu 1 4 88 84 87 89
		f 4 -61 114 115 -110
		mu 0 4 72 77 78 73
		mu 1 4 90 91 92 93
		f 4 -53 -114 116 117
		mu 0 4 79 75 76 80
		mu 1 4 94 88 89 95
		f 4 -60 118 119 -115
		mu 0 4 77 81 82 78
		mu 1 4 91 96 97 92
		f 4 -54 -118 120 121
		mu 0 4 83 79 80 84
		mu 1 4 98 94 95 99
		f 4 -56 122 123 -119
		mu 0 4 81 85 86 82
		mu 1 4 96 100 101 97
		f 4 -55 -122 124 -123
		mu 0 4 85 83 84 86
		mu 1 4 100 98 99 101
		f 4 33 -127 -128 125
		mu 0 4 39 31 16 24
		mu 1 4 44 38 20 32
		f 4 22 23 -130 126
		mu 0 4 31 30 17 16
		mu 1 4 38 37 21 20
		f 4 37 38 -132 -24
		mu 0 4 30 45 26 17
		mu 1 4 37 51 34 21
		f 4 66 67 -134 -39
		mu 0 4 45 55 43 26
		mu 1 4 51 64 49 34
		f 4 80 81 -136 -68
		mu 0 4 55 64 53 43
		mu 1 4 64 75 62 49
		f 4 88 -137 -138 -82
		mu 0 4 64 61 48 53
		mu 1 4 75 70 55 62
		f 4 74 -139 -140 136
		mu 0 4 61 51 49 48
		mu 1 4 70 60 56 55
		f 4 -141 54 145 57
		mu 0 4 49 90 91 103
		mu 1 4 56 102 103 28
		f 4 142 -126 -142 61
		mu 0 4 37 39 24 92
		mu 1 4 42 44 32 104
		f 4 141 -35 -144 60
		mu 0 4 92 24 14 40
		mu 1 4 104 32 18 105
		f 4 -145 53 140 138
		mu 0 4 51 88 90 49
		mu 1 4 60 106 102 56
		f 3 -146 55 56
		mu 0 3 103 91 97
		mu 1 3 28 103 46
		f 4 -147 52 144 58
		mu 0 4 87 104 88 51
		mu 1 4 59 107 106 60;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "uiConfigurationScriptNode";
	setAttr ".b" -type "string" (
		"// Maya Mel UI Configuration File.\n//\n//  This script is machine generated.  Edit at your own risk.\n//\n//\n\nglobal string $gMainPane;\nif (`paneLayout -exists $gMainPane`) {\n\n\tglobal int $gUseScenePanelConfig;\n\tint    $useSceneConfig = $gUseScenePanelConfig;\n\tint    $menusOkayInPanels = `optionVar -q allowMenusInPanels`;\tint    $nVisPanes = `paneLayout -q -nvp $gMainPane`;\n\tint    $nPanes = 0;\n\tstring $editorName;\n\tstring $panelName;\n\tstring $itemFilterName;\n\tstring $panelConfig;\n\n\t//\n\t//  get current state of the UI\n\t//\n\tsceneUIReplacement -update $gMainPane;\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Top View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"top\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n"
		+ "                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n"
		+ "                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n"
		+ "                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Top View\")) -mbv $menusOkayInPanels  $panelName;\n"
		+ "\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"top\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n"
		+ "            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n"
		+ "            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Side View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"side\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n"
		+ "                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n"
		+ "                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n"
		+ "            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Side View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"side\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n"
		+ "            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n"
		+ "            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n"
		+ "\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Front View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"front\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 0\n                -smoothWireframe 0\n                -lineWidth 1\n"
		+ "                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n"
		+ "                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n"
		+ "                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Front View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"front\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n"
		+ "            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 0\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n"
		+ "            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n"
		+ "            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"modelPanel\" (localizedPanelLabel(\"Persp View\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `modelPanel -unParent -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            modelEditor -e \n                -camera \"persp\" \n                -useInteractiveMode 0\n                -displayLights \"default\" \n                -displayAppearance \"smoothShaded\" \n                -activeOnly 0\n                -ignorePanZoom 0\n                -wireframeOnShaded 0\n                -headsUpDisplay 1\n                -selectionHiliteDisplay 1\n                -useDefaultMaterial 0\n"
		+ "                -bufferMode \"double\" \n                -twoSidedLighting 0\n                -backfaceCulling 0\n                -xray 0\n                -jointXray 0\n                -activeComponentsXray 0\n                -displayTextures 1\n                -smoothWireframe 0\n                -lineWidth 1\n                -textureAnisotropic 0\n                -textureHilight 1\n                -textureSampling 2\n                -textureDisplay \"modulate\" \n                -textureMaxSize 16384\n                -fogging 0\n                -fogSource \"fragment\" \n                -fogMode \"linear\" \n                -fogStart 0\n                -fogEnd 100\n                -fogDensity 0.1\n                -fogColor 0.5 0.5 0.5 1 \n                -maxConstantTransparency 1\n                -rendererName \"vp2Renderer\" \n                -objectFilterShowInHUD 1\n                -isFiltered 0\n                -colorResolution 256 256 \n                -bumpResolution 512 512 \n                -textureCompression 0\n                -transparencyAlgorithm \"frontAndBackCull\" \n"
		+ "                -transpInShadows 0\n                -cullingOverride \"none\" \n                -lowQualityLighting 0\n                -maximumNumHardwareLights 1\n                -occlusionCulling 0\n                -shadingModel 0\n                -useBaseRenderer 0\n                -useReducedRenderer 0\n                -smallObjectCulling 0\n                -smallObjectThreshold -1 \n                -interactiveDisableShadows 0\n                -interactiveBackFaceCull 0\n                -sortTransparent 1\n                -nurbsCurves 1\n                -nurbsSurfaces 1\n                -polymeshes 1\n                -subdivSurfaces 1\n                -planes 1\n                -lights 1\n                -cameras 1\n                -controlVertices 1\n                -hulls 1\n                -grid 1\n                -imagePlane 1\n                -joints 1\n                -ikHandles 1\n                -deformers 1\n                -dynamics 1\n                -particleInstancers 1\n                -fluids 1\n                -hairSystems 1\n"
		+ "                -follicles 1\n                -nCloths 1\n                -nParticles 1\n                -nRigids 1\n                -dynamicConstraints 1\n                -locators 1\n                -manipulators 1\n                -pluginShapes 1\n                -dimensions 1\n                -handles 1\n                -pivots 1\n                -textures 1\n                -strokes 1\n                -motionTrails 1\n                -clipGhosts 1\n                -greasePencils 1\n                -shadows 0\n                $editorName;\n            modelEditor -e -viewSelected 0 $editorName;\n            modelEditor -e \n                -pluginObjects \"gpuCacheDisplayFilter\" 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tmodelPanel -edit -l (localizedPanelLabel(\"Persp View\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        modelEditor -e \n            -camera \"persp\" \n            -useInteractiveMode 0\n            -displayLights \"default\" \n            -displayAppearance \"smoothShaded\" \n"
		+ "            -activeOnly 0\n            -ignorePanZoom 0\n            -wireframeOnShaded 0\n            -headsUpDisplay 1\n            -selectionHiliteDisplay 1\n            -useDefaultMaterial 0\n            -bufferMode \"double\" \n            -twoSidedLighting 0\n            -backfaceCulling 0\n            -xray 0\n            -jointXray 0\n            -activeComponentsXray 0\n            -displayTextures 1\n            -smoothWireframe 0\n            -lineWidth 1\n            -textureAnisotropic 0\n            -textureHilight 1\n            -textureSampling 2\n            -textureDisplay \"modulate\" \n            -textureMaxSize 16384\n            -fogging 0\n            -fogSource \"fragment\" \n            -fogMode \"linear\" \n            -fogStart 0\n            -fogEnd 100\n            -fogDensity 0.1\n            -fogColor 0.5 0.5 0.5 1 \n            -maxConstantTransparency 1\n            -rendererName \"vp2Renderer\" \n            -objectFilterShowInHUD 1\n            -isFiltered 0\n            -colorResolution 256 256 \n            -bumpResolution 512 512 \n"
		+ "            -textureCompression 0\n            -transparencyAlgorithm \"frontAndBackCull\" \n            -transpInShadows 0\n            -cullingOverride \"none\" \n            -lowQualityLighting 0\n            -maximumNumHardwareLights 1\n            -occlusionCulling 0\n            -shadingModel 0\n            -useBaseRenderer 0\n            -useReducedRenderer 0\n            -smallObjectCulling 0\n            -smallObjectThreshold -1 \n            -interactiveDisableShadows 0\n            -interactiveBackFaceCull 0\n            -sortTransparent 1\n            -nurbsCurves 1\n            -nurbsSurfaces 1\n            -polymeshes 1\n            -subdivSurfaces 1\n            -planes 1\n            -lights 1\n            -cameras 1\n            -controlVertices 1\n            -hulls 1\n            -grid 1\n            -imagePlane 1\n            -joints 1\n            -ikHandles 1\n            -deformers 1\n            -dynamics 1\n            -particleInstancers 1\n            -fluids 1\n            -hairSystems 1\n            -follicles 1\n            -nCloths 1\n"
		+ "            -nParticles 1\n            -nRigids 1\n            -dynamicConstraints 1\n            -locators 1\n            -manipulators 1\n            -pluginShapes 1\n            -dimensions 1\n            -handles 1\n            -pivots 1\n            -textures 1\n            -strokes 1\n            -motionTrails 1\n            -clipGhosts 1\n            -greasePencils 1\n            -shadows 0\n            $editorName;\n        modelEditor -e -viewSelected 0 $editorName;\n        modelEditor -e \n            -pluginObjects \"gpuCacheDisplayFilter\" 1 \n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"outlinerPanel\" (localizedPanelLabel(\"Outliner\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `outlinerPanel -unParent -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels `;\n\t\t\t$editorName = $panelName;\n            outlinerEditor -e \n                -docTag \"isolOutln_fromSeln\" \n                -showShapes 0\n"
		+ "                -showReferenceNodes 1\n                -showReferenceMembers 1\n                -showAttributes 0\n                -showConnected 0\n                -showAnimCurvesOnly 0\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 1\n                -showAssets 1\n                -showContainedOnly 1\n                -showPublishedAsConnected 0\n                -showContainerContents 1\n                -ignoreDagHierarchy 0\n                -expandConnections 0\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 0\n                -highlightActive 1\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"defaultSetFilter\" \n                -showSetMembers 1\n"
		+ "                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 0\n                -ignoreHiddenAttribute 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\toutlinerPanel -edit -l (localizedPanelLabel(\"Outliner\")) -mbv $menusOkayInPanels  $panelName;\n\t\t$editorName = $panelName;\n        outlinerEditor -e \n            -docTag \"isolOutln_fromSeln\" \n"
		+ "            -showShapes 0\n            -showReferenceNodes 1\n            -showReferenceMembers 1\n            -showAttributes 0\n            -showConnected 0\n            -showAnimCurvesOnly 0\n            -showMuteInfo 0\n            -organizeByLayer 1\n            -showAnimLayerWeight 1\n            -autoExpandLayers 1\n            -autoExpand 0\n            -showDagOnly 1\n            -showAssets 1\n            -showContainedOnly 1\n            -showPublishedAsConnected 0\n            -showContainerContents 1\n            -ignoreDagHierarchy 0\n            -expandConnections 0\n            -showUpstreamCurves 1\n            -showUnitlessCurves 1\n            -showCompounds 1\n            -showLeafs 1\n            -showNumericAttrsOnly 0\n            -highlightActive 1\n            -autoSelectNewObjects 0\n            -doNotSelectNewObjects 0\n            -dropIsParent 1\n            -transmitFilters 0\n            -setFilter \"defaultSetFilter\" \n            -showSetMembers 1\n            -allowMultiSelection 1\n            -alwaysToggleSelect 0\n"
		+ "            -directSelect 0\n            -displayMode \"DAG\" \n            -expandObjects 0\n            -setsIgnoreFilters 1\n            -containersIgnoreFilters 0\n            -editAttrName 0\n            -showAttrValues 0\n            -highlightSecondary 0\n            -showUVAttrsOnly 0\n            -showTextureNodesOnly 0\n            -attrAlphaOrder \"default\" \n            -animLayerFilterOptions \"allAffecting\" \n            -sortOrder \"none\" \n            -longNames 0\n            -niceNames 1\n            -showNamespace 1\n            -showPinIcons 0\n            -mapMotionTrails 0\n            -ignoreHiddenAttribute 0\n            $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\tif ($useSceneConfig) {\n\t\toutlinerPanel -e -to $panelName;\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"graphEditor\" (localizedPanelLabel(\"Graph Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"graphEditor\" -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n"
		+ "                -transmitFilters 1\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Graph Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n"
		+ "                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 1\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 1\n                -showCompounds 0\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 1\n                -doNotSelectNewObjects 0\n                -dropIsParent 1\n                -transmitFilters 1\n                -setFilter \"0\" \n"
		+ "                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 1\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"GraphEd\");\n            animCurveEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 1\n"
		+ "                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -showResults \"off\" \n                -showBufferCurves \"off\" \n                -smoothness \"fine\" \n                -resultSamples 1.25\n                -resultScreenSamples 0\n                -resultUpdate \"delayed\" \n                -showUpstreamCurves 1\n                -stackedCurves 0\n                -stackedCurvesMin -1\n                -stackedCurvesMax 1\n                -stackedCurvesSpace 0.2\n                -displayNormalized 0\n                -preSelectionHighlight 0\n                -constrainDrag 0\n                -classicMode 1\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dopeSheetPanel\" (localizedPanelLabel(\"Dope Sheet\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dopeSheetPanel\" -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n"
		+ "                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n"
		+ "                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dope Sheet\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"OutlineEd\");\n            outlinerEditor -e \n                -showShapes 1\n                -showReferenceNodes 0\n                -showReferenceMembers 0\n                -showAttributes 1\n                -showConnected 1\n                -showAnimCurvesOnly 1\n                -showMuteInfo 0\n                -organizeByLayer 1\n                -showAnimLayerWeight 1\n                -autoExpandLayers 1\n"
		+ "                -autoExpand 0\n                -showDagOnly 0\n                -showAssets 1\n                -showContainedOnly 0\n                -showPublishedAsConnected 0\n                -showContainerContents 0\n                -ignoreDagHierarchy 0\n                -expandConnections 1\n                -showUpstreamCurves 1\n                -showUnitlessCurves 0\n                -showCompounds 1\n                -showLeafs 1\n                -showNumericAttrsOnly 1\n                -highlightActive 0\n                -autoSelectNewObjects 0\n                -doNotSelectNewObjects 1\n                -dropIsParent 1\n                -transmitFilters 0\n                -setFilter \"0\" \n                -showSetMembers 0\n                -allowMultiSelection 1\n                -alwaysToggleSelect 0\n                -directSelect 0\n                -displayMode \"DAG\" \n                -expandObjects 0\n                -setsIgnoreFilters 1\n                -containersIgnoreFilters 0\n                -editAttrName 0\n                -showAttrValues 0\n"
		+ "                -highlightSecondary 0\n                -showUVAttrsOnly 0\n                -showTextureNodesOnly 0\n                -attrAlphaOrder \"default\" \n                -animLayerFilterOptions \"allAffecting\" \n                -sortOrder \"none\" \n                -longNames 0\n                -niceNames 1\n                -showNamespace 1\n                -showPinIcons 0\n                -mapMotionTrails 1\n                -ignoreHiddenAttribute 0\n                $editorName;\n\n\t\t\t$editorName = ($panelName+\"DopeSheetEd\");\n            dopeSheetEditor -e \n                -displayKeys 1\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"integer\" \n                -snapValue \"none\" \n                -outliner \"dopeSheetPanel1OutlineEd\" \n                -showSummary 1\n                -showScene 0\n                -hierarchyBelow 0\n                -showTicks 1\n                -selectionWindow 0 0 0 0 \n"
		+ "                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"clipEditorPanel\" (localizedPanelLabel(\"Trax Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"clipEditorPanel\" -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Trax Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = clipEditorNameFromPanel($panelName);\n"
		+ "            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 0 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"sequenceEditorPanel\" (localizedPanelLabel(\"Camera Sequencer\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"sequenceEditorPanel\" -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n"
		+ "                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Camera Sequencer\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = sequenceEditorNameFromPanel($panelName);\n            clipEditor -e \n                -displayKeys 0\n                -displayTangents 0\n                -displayActiveKeys 0\n                -displayActiveKeyTangents 0\n                -displayInfinities 0\n                -autoFit 0\n                -snapTime \"none\" \n                -snapValue \"none\" \n                -manageSequencer 1 \n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperGraphPanel\" (localizedPanelLabel(\"Hypergraph Hierarchy\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperGraphPanel\" -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels `;\n"
		+ "\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n"
		+ "                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypergraph Hierarchy\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"HyperGraphEd\");\n            hyperGraph -e \n                -graphLayoutStyle \"hierarchicalLayout\" \n                -orientation \"horiz\" \n                -mergeConnections 0\n                -zoom 1\n                -animateTransition 0\n                -showRelationships 1\n                -showShapes 0\n                -showDeformers 0\n                -showExpressions 0\n                -showConstraints 0\n                -showConnectionFromSelected 0\n                -showConnectionToSelected 0\n                -showConstraintLabels 0\n                -showUnderworld 0\n                -showInvisible 0\n                -transitionFrames 1\n                -opaqueContainers 0\n                -freeform 0\n"
		+ "                -imagePosition 0 0 \n                -imageScale 1\n                -imageEnabled 0\n                -graphType \"DAG\" \n                -heatMapDisplay 0\n                -updateSelection 1\n                -updateNodeAdded 1\n                -useDrawOverrideColor 0\n                -limitGraphTraversal -1\n                -range 0 0 \n                -iconSize \"smallIcons\" \n                -showCachedConnections 0\n                $editorName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"hyperShadePanel\" (localizedPanelLabel(\"Hypershade\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"hyperShadePanel\" -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Hypershade\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n"
		+ "\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"visorPanel\" (localizedPanelLabel(\"Visor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"visorPanel\" -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Visor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"nodeEditorPanel\" (localizedPanelLabel(\"Node Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"nodeEditorPanel\" -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels `;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n"
		+ "                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Node Editor\")) -mbv $menusOkayInPanels  $panelName;\n\n\t\t\t$editorName = ($panelName+\"NodeEditorEd\");\n            nodeEditor -e \n                -allAttributes 0\n                -allNodes 0\n"
		+ "                -autoSizeNodes 1\n                -createNodeCommand \"nodeEdCreateNodeCommand\" \n                -defaultPinnedState 0\n                -ignoreAssets 1\n                -additiveGraphingMode 0\n                -settingsChangedCallback \"nodeEdSyncControls\" \n                -traversalDepthLimit -1\n                -keyPressCommand \"nodeEdKeyPressCommand\" \n                -keyReleaseCommand \"nodeEdKeyReleaseCommand\" \n                -nodeTitleMode \"name\" \n                -gridSnap 0\n                -gridVisibility 1\n                -popupMenuScript \"nodeEdBuildPanelMenus\" \n                -showNamespace 1\n                -showShapes 1\n                -showSGShapes 0\n                -showTransforms 1\n                -useAssets 1\n                -syncedSelection 1\n                -extendToShapes 1\n                $editorName;;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"createNodePanel\" (localizedPanelLabel(\"Create Node\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"createNodePanel\" -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Create Node\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"polyTexturePlacementPanel\" (localizedPanelLabel(\"UV Texture Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"polyTexturePlacementPanel\" -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"UV Texture Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"renderWindowPanel\" (localizedPanelLabel(\"Render View\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"renderWindowPanel\" -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Render View\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextPanel \"blendShapePanel\" (localizedPanelLabel(\"Blend Shape\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\tblendShapePanel -unParent -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels ;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tblendShapePanel -edit -l (localizedPanelLabel(\"Blend Shape\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynRelEdPanel\" (localizedPanelLabel(\"Dynamic Relationships\")) `;\n\tif (\"\" == $panelName) {\n"
		+ "\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynRelEdPanel\" -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Dynamic Relationships\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"relationshipPanel\" (localizedPanelLabel(\"Relationship Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"relationshipPanel\" -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Relationship Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"referenceEditorPanel\" (localizedPanelLabel(\"Reference Editor\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"referenceEditorPanel\" -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Reference Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"componentEditorPanel\" (localizedPanelLabel(\"Component Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"componentEditorPanel\" -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Component Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"dynPaintScriptedPanelType\" (localizedPanelLabel(\"Paint Effects\")) `;\n"
		+ "\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"dynPaintScriptedPanelType\" -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Paint Effects\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\t$panelName = `sceneUIReplacement -getNextScriptedPanel \"scriptEditorPanel\" (localizedPanelLabel(\"Script Editor\")) `;\n\tif (\"\" == $panelName) {\n\t\tif ($useSceneConfig) {\n\t\t\t$panelName = `scriptedPanel -unParent  -type \"scriptEditorPanel\" -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels `;\n\t\t}\n\t} else {\n\t\t$label = `panel -q -label $panelName`;\n\t\tscriptedPanel -edit -l (localizedPanelLabel(\"Script Editor\")) -mbv $menusOkayInPanels  $panelName;\n\t\tif (!$useSceneConfig) {\n\t\t\tpanel -e -l $label $panelName;\n\t\t}\n\t}\n\n\n\tif ($useSceneConfig) {\n        string $configName = `getPanel -cwl (localizedPanelLabel(\"Current Layout\"))`;\n"
		+ "        if (\"\" != $configName) {\n\t\t\tpanelConfiguration -edit -label (localizedPanelLabel(\"Current Layout\")) \n\t\t\t\t-defaultImage \"vacantCell.xP:/\"\n\t\t\t\t-image \"\"\n\t\t\t\t-sc false\n\t\t\t\t-configString \"global string $gMainPane; paneLayout -e -cn \\\"vertical2\\\" -ps 1 50 100 -ps 2 50 100 $gMainPane;\"\n\t\t\t\t-removeAllPanels\n\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"Persp View\")) \n\t\t\t\t\t\"modelPanel\"\n"
		+ "\t\t\t\t\t\"$panelName = `modelPanel -unParent -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels `;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t\t\"modelPanel -edit -l (localizedPanelLabel(\\\"Persp View\\\")) -mbv $menusOkayInPanels  $panelName;\\n$editorName = $panelName;\\nmodelEditor -e \\n    -cam `findStartUpCamera persp` \\n    -useInteractiveMode 0\\n    -displayLights \\\"default\\\" \\n    -displayAppearance \\\"smoothShaded\\\" \\n    -activeOnly 0\\n    -ignorePanZoom 0\\n    -wireframeOnShaded 0\\n    -headsUpDisplay 1\\n    -selectionHiliteDisplay 1\\n    -useDefaultMaterial 0\\n    -bufferMode \\\"double\\\" \\n    -twoSidedLighting 0\\n    -backfaceCulling 0\\n    -xray 0\\n    -jointXray 0\\n    -activeComponentsXray 0\\n    -displayTextures 1\\n    -smoothWireframe 0\\n    -lineWidth 1\\n    -textureAnisotropic 0\\n    -textureHilight 1\\n    -textureSampling 2\\n    -textureDisplay \\\"modulate\\\" \\n    -textureMaxSize 16384\\n    -fogging 0\\n    -fogSource \\\"fragment\\\" \\n    -fogMode \\\"linear\\\" \\n    -fogStart 0\\n    -fogEnd 100\\n    -fogDensity 0.1\\n    -fogColor 0.5 0.5 0.5 1 \\n    -maxConstantTransparency 1\\n    -rendererName \\\"vp2Renderer\\\" \\n    -objectFilterShowInHUD 1\\n    -isFiltered 0\\n    -colorResolution 256 256 \\n    -bumpResolution 512 512 \\n    -textureCompression 0\\n    -transparencyAlgorithm \\\"frontAndBackCull\\\" \\n    -transpInShadows 0\\n    -cullingOverride \\\"none\\\" \\n    -lowQualityLighting 0\\n    -maximumNumHardwareLights 1\\n    -occlusionCulling 0\\n    -shadingModel 0\\n    -useBaseRenderer 0\\n    -useReducedRenderer 0\\n    -smallObjectCulling 0\\n    -smallObjectThreshold -1 \\n    -interactiveDisableShadows 0\\n    -interactiveBackFaceCull 0\\n    -sortTransparent 1\\n    -nurbsCurves 1\\n    -nurbsSurfaces 1\\n    -polymeshes 1\\n    -subdivSurfaces 1\\n    -planes 1\\n    -lights 1\\n    -cameras 1\\n    -controlVertices 1\\n    -hulls 1\\n    -grid 1\\n    -imagePlane 1\\n    -joints 1\\n    -ikHandles 1\\n    -deformers 1\\n    -dynamics 1\\n    -particleInstancers 1\\n    -fluids 1\\n    -hairSystems 1\\n    -follicles 1\\n    -nCloths 1\\n    -nParticles 1\\n    -nRigids 1\\n    -dynamicConstraints 1\\n    -locators 1\\n    -manipulators 1\\n    -pluginShapes 1\\n    -dimensions 1\\n    -handles 1\\n    -pivots 1\\n    -textures 1\\n    -strokes 1\\n    -motionTrails 1\\n    -clipGhosts 1\\n    -greasePencils 1\\n    -shadows 0\\n    $editorName;\\nmodelEditor -e -viewSelected 0 $editorName;\\nmodelEditor -e \\n    -pluginObjects \\\"gpuCacheDisplayFilter\\\" 1 \\n    $editorName\"\n"
		+ "\t\t\t\t-ap false\n\t\t\t\t\t(localizedPanelLabel(\"UV Texture Editor\")) \n\t\t\t\t\t\"scriptedPanel\"\n\t\t\t\t\t\"$panelName = `scriptedPanel -unParent  -type \\\"polyTexturePlacementPanel\\\" -l (localizedPanelLabel(\\\"UV Texture Editor\\\")) -mbv $menusOkayInPanels `\"\n\t\t\t\t\t\"scriptedPanel -edit -l (localizedPanelLabel(\\\"UV Texture Editor\\\")) -mbv $menusOkayInPanels  $panelName\"\n\t\t\t\t$configName;\n\n            setNamedPanelLayout (localizedPanelLabel(\"Current Layout\"));\n        }\n\n        panelHistory -e -clear mainPanelHistory;\n        setFocus `paneLayout -q -p1 $gMainPane`;\n        sceneUIReplacement -deleteRemaining;\n        sceneUIReplacement -clear;\n\t}\n\n\ngrid -spacing 100 -size 500 -divisions 10 -displayAxes yes -displayGridLines yes -displayDivisionLines yes -displayPerspectiveLabels no -displayOrthographicLabels no -displayAxesBold yes -perspectiveLabelPosition axis -orthographicLabelPosition edge;\nviewManip -drawCompass 0 -compassAngle 0 -frontParameters \"\" -homeParameters \"\" -selectionLockParameters \"\";\n}\n");
	setAttr ".st" 3;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 0 -max 100 -ast 0 -aet 100 ";
	setAttr ".st" 6;
createNode phong -n "MT_CylinderLamp";
createNode shadingEngine -n "phong1SG";
	setAttr ".ihi" 0;
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode file -n "file1";
	setAttr ".ftn" -type "string" "D:/Unreal Projects/VirtualMuseum/Assets//sourceimages/T_CylinderLamp.png";
createNode place2dTexture -n "place2dTexture1";
createNode polyMapCut -n "polyMapCut1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[34]" "e[125]";
	setAttr ".uvs" -type "string" "lightmap";
createNode polyTweakUV -n "polyTweakUV1";
	setAttr ".uopa" yes;
	setAttr -s 7 ".uvtk";
	setAttr ".uvtk[32]" -type "float2" -0.0024947308 0.024115782 ;
	setAttr ".uvtk[42]" -type "float2" -0.0024947308 0.024115782 ;
	setAttr ".uvtk[43]" -type "float2" -0.0024947308 0.024115782 ;
	setAttr ".uvtk[44]" -type "float2" -0.0024947308 0.024115782 ;
	setAttr ".uvtk[104]" -type "float2" -0.0024947308 0.024115782 ;
	setAttr ".uvtk[105]" -type "float2" -0.0024947308 0.024115782 ;
	setAttr ".uvtk[110]" -type "float2" -0.0024947308 0.024115782 ;
	setAttr ".uvs" -type "string" "lightmap";
createNode polyMapSewMove -n "polyMapSewMove1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 2 "e[143]" "e[146]";
	setAttr ".uvs" -type "string" "lightmap";
createNode polyTweakUV -n "polyTweakUV2";
	setAttr ".uopa" yes;
	setAttr -s 71 ".uvtk";
	setAttr ".uvtk[0]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[1]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[2]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[3]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[4]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[5]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[6]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[7]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[8]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[9]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[14]" -type "float2" -0.0051596928 0.010319385 ;
	setAttr ".uvtk[15]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[16]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[17]" -type "float2" -0.0051596928 0.010319385 ;
	setAttr ".uvtk[18]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[19]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[20]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[21]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[22]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[23]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[26]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[27]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[28]" -type "float2" -0.0051596928 0.010319385 ;
	setAttr ".uvtk[29]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[32]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[33]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[34]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[35]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[36]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[37]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[38]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[39]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[40]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[41]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[42]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[43]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[44]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[45]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[46]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[47]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[48]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[49]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[50]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[51]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[52]" -type "float2" -0.0051596966 0.01031937 ;
	setAttr ".uvtk[53]" -type "float2" -0.0051596966 0.01031937 ;
	setAttr ".uvtk[55]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[56]" -type "float2" -0.0051596928 0.010319385 ;
	setAttr ".uvtk[57]" -type "float2" -0.0051596928 0.010319385 ;
	setAttr ".uvtk[58]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[59]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[60]" -type "float2" -0.0051596928 0.010319385 ;
	setAttr ".uvtk[61]" -type "float2" -0.0051596966 0.01031937 ;
	setAttr ".uvtk[62]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[63]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[64]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[65]" -type "float2" -0.0051596966 0.01031937 ;
	setAttr ".uvtk[69]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[70]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[71]" -type "float2" -0.0051596966 0.010319385 ;
	setAttr ".uvtk[72]" -type "float2" -0.0051596966 0.010319392 ;
	setAttr ".uvtk[74]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[75]" -type "float2" -0.0051596928 0.0103194 ;
	setAttr ".uvtk[76]" -type "float2" -0.0051596966 0.0103194 ;
	setAttr ".uvtk[78]" -type "float2" -0.0051596966 0.0103194 ;
	setAttr ".uvtk[80]" -type "float2" -0.0051596966 0.0103194 ;
	setAttr ".uvtk[102]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[103]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[104]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvtk[105]" -type "float2" -0.0051596928 0.01031937 ;
	setAttr ".uvtk[106]" -type "float2" -0.0051596928 0.010319392 ;
	setAttr ".uvs" -type "string" "lightmap";
createNode polyCopyUV -n "polyCopyUV1";
	setAttr ".uopa" yes;
	setAttr ".ics" -type "componentList" 1 "f[*]";
	setAttr ".uvi" -type "string" "lightmap";
select -ne :time1;
	setAttr ".o" 0;
select -ne :renderPartition;
	setAttr -s 3 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 3 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :defaultRenderUtilityList1;
select -ne :defaultRenderingList1;
select -ne :defaultTextureList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultResolution;
	setAttr ".pa" 1;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :hardwareRenderingGlobals;
	setAttr ".otfna" -type "stringArray" 22 "NURBS Curves" "NURBS Surfaces" "Polygons" "Subdiv Surface" "Particles" "Particle Instance" "Fluids" "Strokes" "Image Planes" "UI" "Lights" "Cameras" "Locators" "Joints" "IK Handles" "Deformers" "Motion Trails" "Components" "Hair Systems" "Follicles" "Misc. UI" "Ornaments"  ;
	setAttr ".otfva" -type "Int32Array" 22 0 1 1 1 1 1
		 1 1 1 0 0 0 0 0 0 0 0 0
		 0 0 0 0 ;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "polyCopyUV1.out" "pCylinder3Shape.i";
connectAttr "polyTweakUV2.uvtk[0]" "pCylinder3Shape.uvst[1].uvtw";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "phong1SG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "file1.oc" "MT_CylinderLamp.c";
connectAttr "file1.ot" "MT_CylinderLamp.it";
connectAttr "MT_CylinderLamp.oc" "phong1SG.ss";
connectAttr "pCylinder3Shape.iog" "phong1SG.dsm" -na;
connectAttr "phong1SG.msg" "materialInfo1.sg";
connectAttr "MT_CylinderLamp.msg" "materialInfo1.m";
connectAttr "file1.msg" "materialInfo1.t" -na;
connectAttr "place2dTexture1.c" "file1.c";
connectAttr "place2dTexture1.tf" "file1.tf";
connectAttr "place2dTexture1.rf" "file1.rf";
connectAttr "place2dTexture1.mu" "file1.mu";
connectAttr "place2dTexture1.mv" "file1.mv";
connectAttr "place2dTexture1.s" "file1.s";
connectAttr "place2dTexture1.wu" "file1.wu";
connectAttr "place2dTexture1.wv" "file1.wv";
connectAttr "place2dTexture1.re" "file1.re";
connectAttr "place2dTexture1.of" "file1.of";
connectAttr "place2dTexture1.r" "file1.ro";
connectAttr "place2dTexture1.n" "file1.n";
connectAttr "place2dTexture1.vt1" "file1.vt1";
connectAttr "place2dTexture1.vt2" "file1.vt2";
connectAttr "place2dTexture1.vt3" "file1.vt3";
connectAttr "place2dTexture1.vc1" "file1.vc1";
connectAttr "place2dTexture1.o" "file1.uv";
connectAttr "place2dTexture1.ofs" "file1.fs";
connectAttr "polySurfaceShape1.o" "polyMapCut1.ip";
connectAttr "polyMapCut1.out" "polyTweakUV1.ip";
connectAttr "polyTweakUV1.out" "polyMapSewMove1.ip";
connectAttr "polyMapSewMove1.out" "polyTweakUV2.ip";
connectAttr "polyTweakUV2.out" "polyCopyUV1.ip";
connectAttr "phong1SG.pa" ":renderPartition.st" -na;
connectAttr "MT_CylinderLamp.msg" ":defaultShaderList1.s" -na;
connectAttr "place2dTexture1.msg" ":defaultRenderUtilityList1.u" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
connectAttr "file1.msg" ":defaultTextureList1.tx" -na;
dataStructure -fmt "raw" -as "name=externalContentTable:string=node:string=key:string=upath:uint32=upathcrc:string=rpath:string=roles";
applyMetadata -fmt "raw" -v "channel\nname externalContentTable\nstream\nname v1.0\nindexType numeric\nstructure externalContentTable\n0\n\"file1\" \"fileTextureName\" \"D:/Unreal Projects/VirtualMuseum/Assets/sourceimages/T_CylinderLamp.png\" 3406132849 \"D:/Unreal Projects/VirtualMuseum/Assets/sourceimages/T_CylinderLamp.png\" \"sourceImages\"\nendStream\nendChannel\nendAssociations\n" 
		-scn;
// End of SM_CylinderLamp.ma
