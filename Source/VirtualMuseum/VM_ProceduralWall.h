// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "VM_ProceduralWall.generated.h"

USTRUCT()
struct FWallPlane
{
	GENERATED_USTRUCT_BODY()

	FWallPlane();

	UPROPERTY(EditAnywhere, Category = VirtualMuseum, Meta = (MakeEditWidget))
	FVector StartPoint;
	
	UPROPERTY(EditAnywhere, Category = VirtualMuseum, Meta = (MakeEditWidget))
	FVector EndPoint;

	UPROPERTY(EditAnywhere, Category = VirtualMuseum)
	bool bDoubleSide;
};

UCLASS()
class VIRTUALMUSEUM_API AVM_ProceduralWall : public AActor
{
	GENERATED_BODY()

	FBoxSphereBounds WallBounds;
	UInstancedStaticMeshComponent* WallsComponent;
	
public:	
	AVM_ProceduralWall();

	void Rebuild();

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& e) override;
	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& e) override;
#endif

	UPROPERTY(EditAnywhere, Category = VirtualMuseum)
	TArray<FWallPlane> WallPlanes;
};
