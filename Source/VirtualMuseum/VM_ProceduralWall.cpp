#include "VirtualMuseum.h"
#include "VM_ProceduralWall.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"

AVM_ProceduralWall::AVM_ProceduralWall()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh> SM_Wall(
		TEXT("StaticMesh'/Game/VirtualMuseum/Architectural/Meshes/SM_Wall.SM_Wall'"));
	WallsComponent = CreateDefaultSubobject<UInstancedStaticMeshComponent>("InstMesh");
	WallsComponent->SetMobility(EComponentMobility::Static);
	WallsComponent->SetStaticMesh(SM_Wall.Object);
	RootComponent = WallsComponent;

	WallBounds = SM_Wall.Object->GetBounds();
}

void AVM_ProceduralWall::Rebuild()
{
	WallsComponent->ClearInstances();
	for (const auto& w : WallPlanes)
	{
		float Distance = FVector::Dist(w.StartPoint, w.EndPoint);
		int Count = Distance / (WallBounds.BoxExtent.Y * 2.f);
		FVector Step{ 0.f, WallBounds.BoxExtent.Y * 2.f, 0.f };// = (w.EndPoint - w.StartPoint) / static_cast<float>(Count);
		for (int i = 0; i < Count; i++)
		{
			float Depth = w.bDoubleSide ? -5 : 0;
			FRotator Direction = (w.EndPoint - w.StartPoint).Rotation().Add(0, 90, 0);
			FVector LocalPosition = Step * static_cast<float>(i) + FVector{ Depth, WallBounds.BoxExtent.Y, 0.f };
			FVector Position = w.StartPoint - Direction.RotateVector(LocalPosition);
			WallsComponent->AddInstance(FTransform{ Direction, Position, FVector{ 1.f } });
			if (w.bDoubleSide)
			{
				Direction = (w.EndPoint - w.StartPoint).Rotation().Add(0, 90, 0);
				LocalPosition = Step * static_cast<float>(i) + FVector{ -Depth, WallBounds.BoxExtent.Y, 0.f };
				Position = w.StartPoint - Direction.RotateVector(LocalPosition);
				WallsComponent->AddInstance(FTransform{ Direction.Add(0, 180, 0), Position, FVector{ 1.f } });
			}
		}
	}
}

#if WITH_EDITOR
void AVM_ProceduralWall::PostEditChangeProperty(FPropertyChangedEvent& e)
{
	Rebuild();
	Super::PostEditChangeProperty(e);
}

void AVM_ProceduralWall::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& e)
{
	Rebuild();
	Super::PostEditChangeChainProperty(e);
}
#endif

FWallPlane::FWallPlane()
{
	StartPoint = FVector{ 0.f, -100.f, 0.f };
	EndPoint = FVector{ 0.f, 100.f, 0.f };
	bDoubleSide = false;
}
