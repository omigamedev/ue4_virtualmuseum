#include "VirtualMuseum.h"
#include "VM_ProceduralSeparator.h"

AVM_ProceduralSeparator::AVM_ProceduralSeparator()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh> SM_SepTower(TEXT("StaticMesh'/Game/VirtualMuseum/Architectural/Meshes/SM_SeparatorTower.SM_SeparatorTower'"));
	ConstructorHelpers::FObjectFinder<UStaticMesh> SM_SepBelt(TEXT("StaticMesh'/Game/VirtualMuseum/Architectural/Meshes/SM_SeparatorWire.SM_SeparatorWire'"));

	TowerInst = CreateDefaultSubobject<UInstancedStaticMeshComponent>("TowerInst");
	TowerInst->SetMobility(EComponentMobility::Static);
	TowerInst->SetStaticMesh(SM_SepTower.Object);
	RootComponent = TowerInst;

	BeltInst = CreateDefaultSubobject<UInstancedStaticMeshComponent>("BeltInst");
	BeltInst->SetMobility(EComponentMobility::Static);
	BeltInst->SetStaticMesh(SM_SepBelt.Object);
	BeltInst->AttachTo(RootComponent);
}

void AVM_ProceduralSeparator::Rebuild()
{
	TowerInst->ClearInstances();
	BeltInst->ClearInstances();
	for (const auto& w : Separators)
	{
		TowerInst->AddInstance(FTransform{ w.StartPoint });
		TowerInst->AddInstance(FTransform{ w.EndPoint });

		//float Distance = FVector::Dist(w.StartPoint, w.EndPoint);
		//int Count = Distance / (WallBounds.BoxExtent.Y * 2.f);
		//FVector Step{ 0.f, WallBounds.BoxExtent.Y * 2.f, 0.f };// = (w.EndPoint - w.StartPoint) / static_cast<float>(Count);
		//for (int i = 0; i < Count; i++)
		//{
		//	float Depth = w.bDoubleSide ? -5 : 0;
		//	FRotator Direction = (w.EndPoint - w.StartPoint).Rotation().Add(0, 90, 0);
		//	FVector LocalPosition = Step * static_cast<float>(i)+FVector{ Depth, WallBounds.BoxExtent.Y, 0.f };
		//	FVector Position = w.StartPoint - Direction.RotateVector(LocalPosition);
		//	WallsComponent->AddInstance(FTransform{ Direction, Position, FVector{ 1.f } });
		//	if (w.bDoubleSide)
		//	{
		//		Direction = (w.EndPoint - w.StartPoint).Rotation().Add(0, 90, 0);
		//		LocalPosition = Step * static_cast<float>(i)+FVector{ -Depth, WallBounds.BoxExtent.Y, 0.f };
		//		Position = w.StartPoint - Direction.RotateVector(LocalPosition);
		//		WallsComponent->AddInstance(FTransform{ Direction.Add(0, 180, 0), Position, FVector{ 1.f } });
		//	}
		//}
	}

}

#if WITH_EDITOR
void AVM_ProceduralSeparator::PostEditChangeProperty(FPropertyChangedEvent& e)
{

}

void AVM_ProceduralSeparator::PostEditChangeChainProperty(struct FPropertyChangedChainEvent& e)
{

}
#endif

FSeparator::FSeparator()
{
	StartPoint = FVector{ 0.f, -100.f, 0.f };
	EndPoint = FVector{ 0.f, 100.f, 0.f };
}
