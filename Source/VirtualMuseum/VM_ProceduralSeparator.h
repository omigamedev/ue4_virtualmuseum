#pragma once

#include "GameFramework/Actor.h"
#include "VM_ProceduralSeparator.generated.h"


USTRUCT()
struct FSeparator
{
	GENERATED_USTRUCT_BODY()

	FSeparator();

	UPROPERTY(EditAnywhere, Category = VirtualMuseum, Meta = (MakeEditWidget))
	FVector StartPoint;

	UPROPERTY(EditAnywhere, Category = VirtualMuseum, Meta = (MakeEditWidget))
	FVector EndPoint;
};

UCLASS()
class VIRTUALMUSEUM_API AVM_ProceduralSeparator : public AActor
{
	GENERATED_BODY()

	UInstancedStaticMeshComponent* TowerInst;
	UInstancedStaticMeshComponent* BeltInst;

public:	
	AVM_ProceduralSeparator();

	void Rebuild();

#if WITH_EDITOR
	virtual void PostEditChangeProperty(FPropertyChangedEvent& e) override;
	virtual void PostEditChangeChainProperty(struct FPropertyChangedChainEvent& e) override;
#endif

	UPROPERTY(EditAnywhere, Category = VirtualMuseum)
	TArray<FSeparator> Separators;
};
